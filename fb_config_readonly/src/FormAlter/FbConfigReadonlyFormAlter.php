<?php

namespace Drupal\fb_config_readonly\FormAlter;

use Drupal\Core\Form\FormStateInterface;

class FbConfigReadonlyFormAlter {
  
  /**
   * @inheritdoc
   */
  public function formAlter(array &$form, FormStateInterface &$form_state) {
    if (!is_array($form) || !isset($form['#form_id'])) {
      return;
    }
    
    if ($form['#form_id'] != 'menu_edit_form') {
      return;
    }
    
    // Prevent edits to this form system settings.
    hide($form['label']);
    hide($form['id']);
    hide($form['description']);

  }
  
}
