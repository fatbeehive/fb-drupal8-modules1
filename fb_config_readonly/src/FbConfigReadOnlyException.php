<?php

namespace Drupal\fb_config_readonly;

use Drupal\Component\Serialization\Yaml;

class FbConfigReadOnlyException {
  
  /**
   * Store the yml config.
   * @var array
   */
  private $config = [];
  
  /**
   * Constructor.
   *
   * Loads the config exceptions automatically.
   */
  public function __construct() {
    $path = drupal_get_path('module', 'fb_config_readonly');
    $path .= '/fb_config_readonly.exceptions.yml';
    $yaml = Yaml::decode(file_get_contents($path));
    $this->config = $yaml['fb_config_readonly.exceptions'];
  }

  /**
   * Determine if the give string is an exception.
   * 
   * @param string $type
   *   Either 'form_ids' or 'config_names'.
   * @param string $match
   *   The string to match against.
   * 
   * @return bool
   *   Whether the given string to match is an exception.
   */
  public function isException($type, $match) {
    if (isset($this->config[$type]) && $this->config[$type]) {
      foreach ($this->config[$type] as $pattern) {
        if ($this->patternMatch($pattern, $match)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * Match using regex.
   *
   * @param string $pattern
   *   The pattern.
   * @param string $source
   *   The string.
   *
   * @return bool
   *   Whether there is a match. 
   */
  protected function patternMatch($pattern, $source) {
    $pattern = preg_quote($pattern, '/');
    $pattern = str_replace('\*', '.*?', $pattern);
    return (bool) preg_match('/^' . $pattern . '$/i', $source);
  }
  
}
