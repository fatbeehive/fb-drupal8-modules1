<?php

namespace Drupal\fb_config_readonly;

use Symfony\Component\DependencyInjection\Reference;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Overrides CMI storage.
 */
class FbConfigReadonlyServiceProvider implements ServiceProviderInterface, ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {}

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->getParameter('kernel.environment') !== 'install') {
      $storage = $container->getDefinition('config.storage');
      $storage->setClass('Drupal\fb_config_readonly\Config\FbConfigReadonlyStorage');
      
      $event_subscriber = $container->getDefinition('config_readonly_form_subscriber');
      $event_subscriber->setClass('Drupal\fb_config_readonly\EventSubscriber\FbReadOnlyFormSubscriber');
    }
  }

}
