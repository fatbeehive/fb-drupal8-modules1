# Drupal 8 Temporary Modules #

Attempting to contribute all of these as open source modules or patches to existing modules:

### fb_config_readonly - waiting for patch approval ###
Exceptions to config read only for when what one site considers config (ie, version controlled) where another considers that content (ie, not version controlled). A form builder is a good example of a grey area. We want clients to be able to create their own forms.
[https://www.drupal.org/node/2826274](https://www.drupal.org/node/2826274) - hoping for patch approval but module seems to not have recent updates; have applied to become a maintainer.

### fb_contact_storage_access - waiting for patch approval ###
Contact storage has grouped access to settings/config together with the ability to view messages. Our client's need to be able to view the messages but not change the contact storage config.
[https://www.drupal.org/node/2708809#comment-11770774](https://www.drupal.org/node/2708809#comment-11770774) - hoping for approval on this patch to split it apart.

### fb_redirects - discontinued - patch has been accepted and commited [https://www.drupal.org/node/2826014](https://www.drupal.org/node/2826014)
Uninstall the module, update redirects module, update editor permissions for redirects.
### fb_menu - alternative now in rc1 - menu_admin_per_menu ###
### fb_site_settings - discontinued: open sourced ###
### fb_users - discontinued: open sourced ###